import React, { Component } from 'react'
import { BrowserRouter as Router, Link ,NavLink} from 'react-router-dom'
import Route from 'react-router-dom/Route'

const User = ({match}) => {
  return ( <h1> Welcome User {match.params.username} </h1>)
}

export default class App extends Component {
  render() {
    return (
      <Router>
        <div>
          <ul>
            <li>
              <NavLink to="/" activeStyle={
                {color:'green'}
              }>Home</NavLink>
              </li>
            <li>
              <NavLink to="/about">about</NavLink>
            </li>
          </ul>

          <Route path="/" exact strict render={
            () => {
              return ( <h1>Welcome to my Profile</h1>)
            }
          }/>
          <Route path="/about" exact strict render={
            () => {
              return ( <h1>นายธราพงษ์ คงกระพันธ์ 59160359</h1>)
            }
          }/>
          <Route path="/user/:username" exact strict component={User}/>
          
        {/* JSX */}
        {/* <h1>Welcome to profile</h1>
        <p>ชื่อ-สกุล : นายธราพงษ์ คงกระพันธ์</p>
        <p>รหัสนิสิต : 59160359</p> */}
        </div>
      </Router>
    )
  }
}
